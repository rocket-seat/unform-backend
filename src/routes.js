const express = require('express')
const routes = express.Router()

const ProdutosController = require('./controllers/ProdutosController')
routes.get('/produtos', ProdutosController.index);
routes.get('/produtos/:id', ProdutosController.show);
routes.put('/produtos/:id', ProdutosController.update);
routes.delete('/produtos/:id', ProdutosController.destroy);
routes.post('/produtos', ProdutosController.store);

module.exports = routes
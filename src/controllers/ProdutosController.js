const mongoose = require('mongoose')
const Produto = mongoose.model('Produto')

module.exports = {
    async index(req, res) {
        const { page = 1 } = req.query;
        const registros = await Produto.paginate({}, { page, limit: 10 });

        return res.json(registros)
    },
    async show(req, res) {
        const registro = await Produto.findById(req.params.id);

        return res.json(registro)
    },
    async update(req, res) {
        const registro = await Produto.findByIdAndUpdate(req.params.id, req.body, { new: true });

        // Avisa o front-end
        req.io.emit('updProduto', registro);

        return res.json(registro)
    },
    async destroy(req, res) {
        await Produto.findByIdAndRemove(req.params.id);

        // Avisa o front-end
        req.io.emit('delProduto', req.params.id);

        return res.send()
    },
    async store(req, res) {
        const registro = await Produto.create(req.body)

        // Avisa o front-end
        req.io.emit('newProduto', registro);

        return res.json(registro)
    }
}